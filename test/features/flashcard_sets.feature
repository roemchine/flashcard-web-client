Feature: Flashcard Sets
  As a user
  I want to browse through
  all flashcard sets

  Scenario: Visit home page
    Given I open the site "/"
    Then I should see all flashcard sets

  Scenario: Open a flashcard set
    Given I open the site "/"
    When I click on a flashcard set
    Then I should see the flashcard set

  Scenario: Create a flashcard set with correct values
    Given I open the site "/"
    When I click on new
    Then I should see a form to create a flashcard set
    When I set the values of the flashcard set correctly
    Then I should see that the flashcard set has been created corretly
    And I should see the flashcard set

  Scenario: Create a flashcard set with no flashcards

  Scenario: Create a flashcard set with an existing title

  Scenario: Create a flashcard set with too short values

  Scenario: Create a flashcard set with too long values

  Scenario: Edit a flashcard set with correct values
    Given I open the site "/"
    When I click on a flashcard set
    And I click on edit
    Then I should see a form to edit a flashcard set
    When I change the values of the flashcard set correctly
    Then I should see that the flashcard set has been updated corretly
    And I should see the flashcard set

  Scenario: Edit a flashcard set with no flashcards

  Scenario: Edit a flashcard set with an existing title

  Scenario: Edit a flashcard set with too short values

  Scenario: Edit a flashcard set with too long values

  Scenario: Edit a flashcard set when someone has already applied changes
