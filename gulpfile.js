'use strict';

// gulp plugins
var gulp = require('gulp'),
  gutil = require('gulp-util'),
  es = require('event-stream'),
  sass = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  jshint = require('gulp-jshint'),
  del = require('del'),
  connect = require('gulp-connect'),
  usemin = require('gulp-usemin'),
  imagemin = require('gulp-imagemin'),
  concat = require('gulp-concat'),
  sourcemaps = require('gulp-sourcemaps'),
  babelify = require('babelify'),
  buffer = require('vinyl-buffer'),
  source = require('vinyl-source-stream'),
  uglify = require('gulp-uglify'),
  browserify = require('browserify');

// Connect Task
gulp.task('connect', function() {
  connect.server({
    root: ['./dist'],
    port: 3000,
    livereload: true
  })
});

// Html reload
gulp.task('html', function() {
  return gulp.src('./app/**/*.html')
    .pipe(gulp.dest('./dist/'))
    .pipe(connect.reload());
});

// sass compiler task
gulp.task('sass', function() {
  return gulp.src('./app/styles/**/*.scss')
    .pipe(sass({
      includePaths: [
        './app/components/angular-chart.js/dist',
        './app/components/bootstrap-sass/assets/stylesheets',
        './app/components/font-awesome/scss'
      ],
      onError: function(error) {
        gutil.log(gutil.colors.red(error));
        gutil.beep();
      },
      onSuccess: function() {
        gutil.log(gutil.colors.green('Sass styles compiled successfully.'));
      }
    }))
    .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('./dist/styles/'))
    .pipe(connect.reload());
});

// Minify images
gulp.task('imagemin', function() {
  return es.concat(
    gulp.src('./app/images/**/*.png')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/images/')),
    gulp.src('./app/images/**/*.jpg')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/images/')),
    gulp.src('./app/images/**/*.gif')
    .pipe(imagemin())
    .pipe(gulp.dest('./dist/images/'))
  );
});

// CoffeeScript compiler task
gulp.task('coffee', function() {
  return gulp.src('./app/scripts/**/*.coffee')
    .pipe(coffee({
      bare: true
    })).on('error', gutil.log)
    .pipe(gulp.dest('./app/scripts'));
});

// Script task
gulp.task('scripts', function() {
  browserify('./app/scripts/app.js', {
      debug: true
    })
    .add(require.resolve('babel-polyfill'))
    .transform(babelify, {presets: ["es2015"]})
    .bundle()
    .on('error', gutil.log.bind(gutil, 'Browserify Error'))
    .pipe(source('app.js'))
    .pipe(jshint({
      esnext: true,
      globalstrict: true,
      predef: ['angular']
    }))
    .pipe(jshint.reporter('default'))
    .pipe(buffer())
    .pipe(sourcemaps.init({
      loadMaps: true
    }))
    .pipe(uglify({
      mangle: false
    }))
    .pipe(sourcemaps.write('./'))
    .pipe(gulp.dest('./dist/scripts'))
    .pipe(connect.reload());
});

gulp.task('script-components', function() {
  return gulp.src([
    './app/components/jquery/dist/jquery.min.js',
    './app/components/Chart.js/Chart.min.js',
    './app/components/angular/angular.min.js',
    './app/components/angular-messages/angular-messages.min.js',
    './app/components/angular-ui-router/release/angular-ui-router.min.js',
    './app/components/angular-jwt/dist/angular-jwt.min.js',
    './app/components/angular-chart.js/dist/angular-chart.min.js',
    './app/components/bootstrap-sass/assets/javascripts/bootstrap.min.js',
    './app/components/angularUtils-pagination/dirPagination.js',
    './app/components/angular-bootstrap/ui-bootstrap-tpls.min.js',
    './app/components/ng-file-upload/ng-file-upload.min.js'
  ], {
    base: './app/components'
  }).pipe(gulp.dest('./dist/components/'));
});

// Fontawesome fonts
gulp.task('fonts', function() { 
  return gulp.src('./app/components/font-awesome/fonts/**.*') 
    .pipe(gulp.dest('./dist/fonts')); 
});

gulp.task('watch', function() {
  gulp.watch(['./app/styles/**/*.scss'], ['sass']);
  gulp.watch(['./app/scripts/**/*.js'], ['scripts']);
  gulp.watch(['./app/**/*.html'], ['html']);
});

gulp.task('serve', ['connect', 'sass', 'scripts', 'fonts', 'watch']);

gulp.task('clean', function() {
  gutil.log('Clean task goes here...');
});

gulp.task('usemin', function() {
  gulp.src('./app/**/*.html')
    .pipe(usemin())
    .pipe(gulp.dest('./dist/'));
});

gulp.task('clean-build', function() {
  del.sync(['./dist/**']);
});

gulp.task('build', ['clean-build', 'sass', 'script-components', 'scripts', 'fonts', 'imagemin', 'usemin'], function() {});

gulp.task('default', function() {
  gutil.log('Default task goes here...');
});
