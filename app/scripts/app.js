'use strict';

// Libs
import './libs/array_remove_element';
import './libs/angular_uuid';

import {
  Constants
}
from './constants'

// Configs
import {
  RouterConfig
}
from './configs/router_config';

import {
  JwtInterceptorConfig
}
from './configs/jwt_interceptor_config';

// Runs
import {
  DefaultRun
}
from './runs/default_run'

// Services
import {
  RestService
}
from './services/rest_service';

import {
  FileUploadService
}
from './services/file_upload_service'

// Controllers
import {
  MainMenuController
}
from './controllers/main_menu_controller';

import {
  HomeController
}
from './controllers/home_controller';

import {
  EditFlashcardSetController
}
from './controllers/edit_flashcard_set_controller';

import {
  NewFlashcardSetController
}
from './controllers/new_flashcard_set_controller';

import {
  FlashcardSetHistoryController
}
from './controllers/flashcard_set_history_controller'

import {
  FlashcardController
}
from './controllers/flashcard_controller';

import {
  LoginController
}
from './controllers/login_controller';

// Directives
import {
  FlashcardSetPreviewDirective
}
from './directives/flashcard_set_preview_directive';

import {
  FlashcardSetFormDirective
}
from './directives/flashcard_set_form_directive';

angular.module('flashcard', ['ngMessages', 'ui.router', 'angular-jwt', 'chart.js', 'angularUtils.directives.dirPagination', 'ui.bootstrap', 'ngFileUpload', 'angular-uuid']);

// Constants
angular.module('flashcard')
  .constant('constants', Constants.getConstants());

// Configs
angular.module('flashcard')
  .config([
    '$stateProvider', '$urlRouterProvider',
    RouterConfig.getInstance
  ])
  .config([
    '$httpProvider', 'jwtInterceptorProvider', 'constants',
    JwtInterceptorConfig.getInstance
  ])
  .config(function(paginationTemplateProvider) {
    paginationTemplateProvider.setPath('./../components/angularUtils-pagination/dirPagination.tpl.html');
  });

// Runs
angular.module('flashcard')
  .run(['$rootScope', '$state',
    DefaultRun.getInstance
  ]);

// Services
angular.module('flashcard')
  .service('restService', [
    '$http', 'constants',
    RestService.getInstance
  ])
  .service('fileUploadService', [
    '$http', 'Upload', 'constants', 'uuid',
    FileUploadService.getInstance
  ]);

// Controllers
angular.module('flashcard')
  .controller('mainMenuController', [
    '$state', 'jwtHelper',
    MainMenuController.createInstance
  ])
  .controller('homeController', [
    'restService',
    HomeController.createInstance
  ])
  .controller('editFlashcardSetController', [
    '$stateParams', '$state', '$q',
    'restService', 'fileUploadService',
    EditFlashcardSetController.createInstance
  ])
  .controller('newFlashcardSetController', [
    '$state', '$q', 'restService', 'fileUploadService',
    NewFlashcardSetController.createInstance
  ])
  .controller('flashcardSetHistoryController', [
    '$stateParams', '$state', 'restService',
    FlashcardSetHistoryController.createInstance
  ])
  .controller('flashcardController', [
    '$stateParams', '$state', 'restService',
    FlashcardController.createInstance
  ])
  .controller('loginController', [
    '$state', 'restService',
    LoginController.createInstance
  ]);

// Directives
angular.module('flashcard')
  .directive('flashcardSetPreviewDirective', FlashcardSetPreviewDirective.getInstance)
  .directive('flashcardSetFormDirective', ['uuid', FlashcardSetFormDirective.getInstance]);