'use strict';

export class RestService {
  constructor($http, constants) {
    this.http = $http;
    this.baseURL = constants.restService.url;
  }

  sanitizeFlashcardSet(flashcardSet) {
    var clone = {};
    angular.copy(flashcardSet, clone);
    delete clone.flashcardCompartmentInfo;
    clone.flashcards.forEach((flashcard) => {
      delete flashcard.questionImageFile;
      delete flashcard.deleteQuestionImageFile;
      delete flashcard.invalidQuestionImageFiles;
      delete flashcard.answerImageFile;
      delete flashcard.deleteAnswerImageFile;
      delete flashcard.invalidAnswerImageFiles;
    });
    
    console.log(clone);
    return clone;
  }

  getFlashcardSets(limit, skip) {
    return this.http.get(this.baseURL + '/flashcard_sets', {
      params: {
        limit: limit,
        skip: skip
      }
    });
  }

  getFlashcardSet(id) {
    return this.http.get(this.baseURL + '/flashcard_sets/' + id);
  }

  getFlashcardSetRevisions(id, limit, skip) {
    return this.http.get(this.baseURL + '/flashcard_sets/' + id + '/revisions', {
      params: {
        limit: limit,
        skip: skip
      }
    });
  }

  postFlashcardSet(flashcardSet) {
    var sanitizedFlashcardSet = this.sanitizeFlashcardSet(flashcardSet);
    return this.http.post(this.baseURL + '/flashcard_sets/', sanitizedFlashcardSet);
  }

  putFlashcardSet(flashcardSet) {
    var id = flashcardSet._key;
    var sanitizedFlashcardSet = this.sanitizeFlashcardSet(flashcardSet);
    return this.http.put(this.baseURL + '/flashcard_sets/' + id, sanitizedFlashcardSet);
  }

  learnFlashcardSet(id) {
    return this.http.get(this.baseURL + '/flashcard_sets/' + id + '/learn');
  }

  knownFlashcard(id) {
    return this.http.get(this.baseURL + '/flashcards/' + id + '/known');
  }

  unknownFlashcard(id) {
    return this.http.get(this.baseURL + '/flashcards/' + id + '/unknown');
  }

  login(credentials) {
    return this.http.post(this.baseURL + '/users/login', credentials, {skipAuthorization: true});
  }

  static getInstance($http, constants) {
    if (RestService.instance === undefined) {
      RestService.instance = new RestService($http, constants);
    }
    return RestService.instance;
  }
}