'use strict';

export class FileUploadService {
  constructor($http, Upload, constants, uuid) {
    this.$http = $http;
    this.Upload = Upload;
    this.fileUploadServiceConsts = constants.fileUploadService;
    this.uuid = uuid;
  }

  getFileUploadPromises(flashcardSet) {
    var promises = [];
    flashcardSet.flashcards.forEach((flashcard, index, array) => {
      if (flashcard.questionImageFile) {
        promises.push(this.uploadFlashcardQuestionImage(flashcard));
      }

      if (flashcard.deleteQuestionImageFile) {
        promises.push(this.deleteFlashcardQuestionImage(flashcard));
      }

      if (flashcard.answerImageFile) {
        promises.push(this.uploadFlashcardAnswerImage(flashcard));
      }

      if (flashcard.deleteAnswerImageFile) {
        promises.push(this.deleteFlashcardAnswerImage(flashcard));
      }
    });
    return promises;
  }

  uploadFlashcardQuestionImage(flashcard) {
    var questionImageKey = 'uploads/flashcard/questionImage/' + this.uuid.v4();
    flashcard.questionImageUrl = this.fileUploadServiceConsts.url + questionImageKey;
    return this.Upload.upload({
      url: this.fileUploadServiceConsts.url, //S3 upload url including bucket name
      method: 'POST',
      skipAuthorization: true,
      data: {
        key: questionImageKey, // the key to store the file on S3, could be file name or customized
        AWSAccessKeyId: this.fileUploadServiceConsts.AWSAccessKeyId,
        acl: 'public-read', // sets the access to the uploaded file in the bucket: private, public-read, ...
        policy: this.fileUploadServiceConsts.policy, // base64-encoded json policy (see article below)
        signature: this.fileUploadServiceConsts.signature, // base64-encoded signature based on policy string (see article below)
        "Content-Type": flashcard.questionImageFile.type != '' ? flashcard.questionImageFile.type : 'application/octet-stream', // content type of the file (NotEmpty)
        file: flashcard.questionImageFile
      }
    });
  }

  uploadFlashcardAnswerImage(flashcard) {
    var answerImageKey = 'uploads/flashcard/answerImage/' + this.uuid.v4();
    flashcard.answerImageUrl = this.fileUploadServiceConsts.url + answerImageKey;
    return this.Upload.upload({
      url: this.fileUploadServiceConsts.url, //S3 upload url including bucket name
      method: 'POST',
      skipAuthorization: true,
      data: {
        key: answerImageKey, // the key to store the file on S3, could be file name or customized
        AWSAccessKeyId: this.fileUploadServiceConsts.AWSAccessKeyId,
        acl: 'public-read', // sets the access to the uploaded file in the bucket: private, public-read, ...
        policy: this.fileUploadServiceConsts.policy, // base64-encoded json policy (see article below)
        signature: this.fileUploadServiceConsts.signature, // base64-encoded signature based on policy string (see article below)
        "Content-Type": flashcard.answerImageFile.type != '' ? flashcard.answerImageFile.type : 'application/octet-stream', // content type of the file (NotEmpty)
        file: flashcard.answerImageFile
      }
    });
  }

  deleteFlashcardQuestionImage(flashcard) {
    delete flashcard.questionImageUrl;
  }

  deleteFlashcardAnswerImage(flashcard) {
    delete flashcard.answerImageUrl;
  }

  static getInstance($http, Upload, constants, uuid) {
    if (FileUploadService.instance === undefined) {
      FileUploadService.instance = new FileUploadService($http, Upload, constants, uuid);
    }
    return FileUploadService.instance;
  }
}