'use strict';

class FlashcardSetFormDirective {
  constructor(uuid) {
    this.restrict = 'E';
    this.templateUrl = '/directives/flashcard_set_form.html';
    this.replace = true;

    this.scope = {
      onSubmit: '&onsubmit',
      flashcardSet: '=flashcardset'
    };

    this.controller = ($scope) => {
      $scope.addFlashcard = ($event) => {
        $event.preventDefault();
        if ($scope.flashcardSet.flashcards === undefined) {
          $scope.flashcardSet.flashcards = [];
        }
        $scope.flashcardSet.flashcards.push({
          _key: uuid.v4(),
          question: '',
          answer: ''
        });
        console.log($scope.flashcardSet.flashcards);
      }

      $scope.deleteFlashcard = ($event, $index) => {
        $event.preventDefault();
        $scope.flashcardSet.flashcards.remove($index);
      }
    };
  }

  static getInstance(uuid) {
    if (FlashcardSetFormDirective.instance === undefined) {
      FlashcardSetFormDirective.instance = new FlashcardSetFormDirective(uuid);
    }
    return FlashcardSetFormDirective.instance;
  }
}

export {
  FlashcardSetFormDirective
};