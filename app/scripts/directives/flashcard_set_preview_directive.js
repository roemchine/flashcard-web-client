'use strict';

class FlashcardSetPreviewDirective {
  constructor() {
    this.restrict = 'E';
    this.templateUrl = '/directives/flashcard_set_preview.html';
    this.replace = true;
    this.scope = {};
    this.scope.id = '=';
    this.scope.title = '=';
    this.scope.description = '=';
  }

  static getInstance() {
    if (FlashcardSetPreviewDirective.instance === undefined) {
      FlashcardSetPreviewDirective.instance = new FlashcardSetPreviewDirective();
    }
    return FlashcardSetPreviewDirective.instance;
  }
}

export {
  FlashcardSetPreviewDirective
};
