'use strict';

export class DefaultRun {
  constructor($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', (event, toState, toParams, fromState, fromParams) => {
      if (toState.onlyIfAuthenticated && window.localStorage.getItem('JWT') == null) {
        event.preventDefault();
        var requestedUrl = $state.href(toState, toParams); 
            
        $state.transitionTo('login', { requestedUrl: requestedUrl });
      }
    });
  }

  static getInstance($rootScope, $state) {
    if (DefaultRun.instance === undefined) {
      DefaultRun.instance = new DefaultRun($rootScope, $state);
    }
    return DefaultRun.instance;
  }
}