'use strict';

class JwtInterceptorConfig {
  constructor($httpProvider, jwtInterceptorProvider, constants) {
    jwtInterceptorProvider.authPrefix = '';
    jwtInterceptorProvider.tokenGetter = ['config', 'jwtHelper', (config, jwtHelper) => {
      if (config.url.startsWith(constants.fileUploadService.url)) {
        return null;
      }
      return window.localStorage.getItem('JWT');
    }];
    $httpProvider.interceptors.push('jwtInterceptor');
  }

  static getInstance($httpProvider, jwtInterceptorProvider, constants) {
    if (JwtInterceptorConfig.instance === undefined) {
      JwtInterceptorConfig.instance = new JwtInterceptorConfig($httpProvider, jwtInterceptorProvider, constants);
    }
    return JwtInterceptorConfig.instance;
  }
}

export {
  JwtInterceptorConfig
};