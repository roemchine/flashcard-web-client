'use strict';

export class RouterConfig {
  constructor($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/pages/error.html");
    $urlRouterProvider.when('', '/');
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: '/pages/home.html',
        controller: 'homeController'
      })
      .state('flashcard-set-new', {
        url: '/flashcard-set/new',
        templateUrl: '/pages/flashcard_set_new.html',
        controller: 'newFlashcardSetController',
        onlyIfAuthenticated: true
      })
      .state('flashcard-set', {
        abstract: true,
        url: '/flashcard-set/:id',
        templateUrl: '/pages/flashcard_set.html',
        controller: 'editFlashcardSetController'
      }).state('flashcard-set.get', {
        url: '/?message',
        templateUrl: '/pages/flashcard_set_get.html',
        controller: 'editFlashcardSetController',
        onlyIfAuthenticated: true
      }).state('flashcard-set.edit', {
        url: '/edit',
        templateUrl: '/pages/flashcard_set_edit.html',
        controller: 'editFlashcardSetController',
        onlyIfAuthenticated: true
      })
      .state('flashcard-set.history', {
        url: '/history',
        templateUrl: '/pages/flashcard_set_history.html',
        controller: 'flashcardSetHistoryController',
        onlyIfAuthenticated: true
      })
      .state('flashcard-set-learn', {
        url: '/flashcard-set/:id/learn',
        templateUrl: '/pages/flashcard_learn.html',
        controller: 'flashcardController',
        onlyIfAuthenticated: true
      })
      .state('login', {
        url: '/login',
        templateUrl: '/pages/login.html',
        controller: 'loginController'
      });
  }

  static getInstance($stateProvider, $urlRouterProvider) {
    if (RouterConfig.instance === undefined) {
      RouterConfig.instance = new RouterConfig($stateProvider, $urlRouterProvider);
    }
    return RouterConfig.instance;
  }
}
