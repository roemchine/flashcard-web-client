'use strict';

export class EditFlashcardSetController {
  constructor($stateParams, $state, $q, restService, fileUploadService) {
    this.$state = $state;
    this.$q = $q;
    this.restService = restService;
    this.fileUploadService = fileUploadService;
    this.message = $stateParams.message;

    restService.getFlashcardSet($stateParams.id).then((result) => {
      this.flashcardSet = result.data;
      if (this.flashcardSet.swapQuestionsWithAnswersRandomly === undefined) {
        this.flashcardSet.swapQuestionsWithAnswersRandomly = false;
      }
    }).catch((reason) => {
      if (reason.status === 401) {
        this.$state.go('login');
      }
    });
  }

  update() {
    this.$q.all(this.fileUploadService.getFileUploadPromises(this.flashcardSet))
      .then((resp) => {

        this.restService.putFlashcardSet(this.flashcardSet).then((result) => {
          this.$state.go('flashcard-set.get', {
            id: this.flashcardSet._key
          });
        }).catch((reason) => {
          if (reason.status === 401) {
            this.$state.go('login');
          }
        });
      }, (resp) => {
        console.log('Error in file upload');
      }, (evt) => {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
  }

  static createInstance($stateParams, $state, $q, restService, fileUploadService) {
    return new EditFlashcardSetController($stateParams, $state, $q, restService, fileUploadService);
  }
}