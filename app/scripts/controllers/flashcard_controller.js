'use strict';

export class FlashcardController {
  constructor($stateParams, $state, restService) {
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.restService = restService;
    this.answerVisible = false;
    this.defaultFinshedMessage = 'Congratulations! You have successfully learned this flashcard set.'

    restService.learnFlashcardSet($stateParams.id).then((result) => {
      this.flashcard = result.data;
      if (result.status === 204) {
        // learning finished
        this.$state.go('flashcard-set.get', { id: this.$stateParams.id, message: this.defaultFinshedMessage });
      }
    }).catch((reason) => {
      if (reason.status === 401) {
        this.$state.go('login');
      } 
    });
  }

  setAnswerVisible() {
    this.answerVisible = true;
  }

  known() {
    this.restService.knownFlashcard(this.flashcard._key).then((result) => {
      this.answerVisible = false;
      this.flashcard = result.data;
      if (result.status === 204) {
        // learning finished
        this.$state.go('flashcard-set.get', {id: this.$stateParams.id, message: this.defaultFinshedMessage });
      }
    }).catch((reason) => {
      if (reason.status === 401) {
        this.$state.go('login');
      } 
    });
  }

  unknown() {
    this.restService.unknownFlashcard(this.flashcard._key).then((result) => {
      this.answerVisible = false;
      this.flashcard = result.data;
      if (result.status === 204) {
        // learning finished
        this.$state.go('flashcard-set.get', { id: this.$stateParams.id, message: this.defaultFinshedMessage });
      }
    }).catch((reason) => {
      if (reason.status === 401) {
        this.$state.go('login');
      }
    });
  }

  static createInstance($stateParams, $state, restService) {
    return new FlashcardController($stateParams, $state, restService);
  }
}