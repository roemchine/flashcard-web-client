'use strict';

class MainMenuController {
  constructor($state, jwtHelper) {
    this.$state = $state;
    this.jwtHelper = jwtHelper;
  }

  getJwt() {
    return window.localStorage.getItem('JWT');
  }

  logout() {
    window.localStorage.removeItem('JWT');
    this.$state.go('home');
  }

  static createInstance($state, jwtHelper) {
    return new MainMenuController($state, jwtHelper);
  }
}

export {
  MainMenuController
};
