'use strict';

export class FlashcardSetHistoryController {
  constructor($stateParams, $state, restService) {
    this.$stateParams = $stateParams;
    this.$state = $state;
    this.restService = restService;
    this.revisionsPerPage = 20;
    this.pagination = {
        current: 1
    };

    this.getRevisionsPage(1);
  }

  getRevisionsPage(pageNumber) {
    var skip = this.revisionsPerPage * (pageNumber - 1);

    this.restService.getFlashcardSetRevisions(this.$stateParams.id, this.revisionsPerPage, skip).then((result) => {
      this.flashcardSetRevisions = result.data.flashcardSetRevisions;
      this.totalLengthOfFlashcardSetRevisions = result.data.totalLengthOfFlashcardSetRevisions;
    }).catch((reason) => {
      if (reason.status === 401) {
        this.$state.go('login');
      }
    });
  }

  static createInstance($stateParams, $state, restService) {
    return new FlashcardSetHistoryController($stateParams, $state, restService);
  }
}