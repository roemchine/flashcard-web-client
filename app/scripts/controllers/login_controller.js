'use strict';

class LoginController {
  constructor($state, restService) {
    this.$state = $state;
    this.restService = restService;
  }

  login() {
    this.restService.login(this.credentials).then((result) => {
      window.localStorage.setItem('JWT', result.data.token);
      this.$state.go('home');
    }).catch((reason) => {
      this.error = reason.data.error;
    });
  }

  static createInstance($state, restService) {
    return new LoginController($state, restService);
  }
}

export {
  LoginController
};