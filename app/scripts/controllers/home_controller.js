'use strict';

class HomeController {

  constructor(restService) {
    this.restService = restService;
    this.flashcardSetsPerPage = 20;
    this.pagination = {
        current: 1
    };

    this.getFlashcardSetsPage(1);

  }

  getFlashcardSetsPage(pageNumber) {
    var skip = this.flashcardSetsPerPage * (pageNumber - 1);

    this.restService.getFlashcardSets(
      this.flashcardSetsPerPage, skip
    ).then((result) => {
      this.flashcardSets = result.data.flashcardSets;
      this.totalLengthOfFlashcardSets = result.data.totalLengthOfFlashcardSets;
    });
  }


  static createInstance(restService) {
    return new HomeController(restService);
  }
}

export {
  HomeController
};
