'use strict';

export class NewFlashcardSetController {
  constructor($state, $q, restService, fileUploadService) {
    this.$state = $state;
    this.$q = $q;
    this.restService = restService;
    this.fileUploadService = fileUploadService;
    this.flashcardSet = {
      swapQuestionsWithAnswersRandomly: false,
      flashcards: []
    };
  }

  create() {
    this.$q.all(this.fileUploadService.getFileUploadPromises(this.flashcardSet))
      .then((resp) => {
        this.restService.postFlashcardSet(this.flashcardSet).then((result) => {
          this.$state.go('flashcard-set.get', {
            id: result.data._key
          });
        }).catch((reason) => {
          if (reason.status === 401) {
            this.$state.go('login');
          }
        });
      }, (resp) => {
        console.log('Error in file upload');
      }, (evt) => {
        var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
        console.log('progress: ' + progressPercentage + '% ' + evt.config.data.file.name);
      });
  }

  static createInstance($state, $q, restService, fileUploadService) {
    return new NewFlashcardSetController($state, $q, restService, fileUploadService);
  }
}